module DataCol

  ## WHEN MATURE???

  require "net/http"
  require "uri"
  require "json"
  #require "fileutils"
  require "yaml"
  require "mongo"

  CONTRACTS = {this_week: true, next_week: true, quarter: true}

  def config
    conf = YAML::load_file('config.yaml')
    cdata = [conf['server'],conf['user'],conf['pass'],conf['db']]
    return cdata
  end


  def db_con
    begin
      cdata = DataCol::config
      client = Mongo::Client.new(
      [ cdata[0] ],
      user: cdata[1],
      password: cdata[2],
      :database => cdata[3]
      )
#testing!!
#client[:line].find.delete_many
    rescue
    	puts "Error connecting to database."
    end
   return client
  end

  def db_put( data_hash, col )

    if col.count < 1
     col.create
    end

    result = col.insert_one( data_hash )
  	col.find.each do |i|
  	 p i
  	end

  end#db put


 # =============================
 # Okcoin
 #

class Okcoin

  def tickers
    # https://developers.google.com/chart/interactive/docs/gallery/linechart#curving-the-lines
    furi = URI.parse( "https://www.okcoin.com/api/v1/future_ticker.do?symbol=btc_usd&contract_type=this_week" )
    suri = URI.parse( "https://www.okcoin.com/api/v1/ticker.do?symbol=btc_usd" )

    fres = Net::HTTP.get_response(furi)
    fdata = JSON.parse(fres.body)

    sres = Net::HTTP.get_response(suri)
    sdata = JSON.parse(sres.body)

    outer = []
    date = ""
    fsell = ""
    sbuy = ""

    fdata.each do |item|
      if item[0] == "date"
       date = item[1].to_i
      elsif item[0] == "ticker"
       fsell = item[1]['sell']
      end
    end

    sdata.each do |item|
      if item[0] == "ticker"
       sbuy = item[1]['buy']
      end
    end

    outer.push(date)
    outer.push(fsell)
    outer.push(sbuy)
    #p outer

    # each entry should make a line in a collection
    # - change collections on sunday
    # get json from db


    jf = File.read( "public/premium.json")
    old = JSON.parse( jf )
    all = old.push( outer ).to_json
    p all
    #jf.write( all )
    #jf.close
    return outer
  end


 def parser_future( contract ) #kandle
  tocall = "https://www.okcoin.com/api/v1/future_kline.do?symbol=btc_usd&type=15min&contract_type=" + contract.to_s
  self.response( tocall, contract )
 end

 def parser_spot
   tocall = "https://www.okcoin.com/api/v1/kline.do?symbol=btc_usd&type=15min" #only about 2 weeks
   self.response( tocall, "spot" )
 end


 def forecast
  tocall = "https://www.okcoin.com/api/v1/future_estimated_price.do?symbol=btc_usd"
  uri = URI.parse( tocall )
  response = Net::HTTP.get_response(uri)
  data = JSON.parse(response.body)
  forecast = data['forecast_price']
  return forecast
 end

 def response( tocall, contract )

  puts "Calling #{ tocall }"

  uri = URI.parse( tocall )
  response = Net::HTTP.get_response( uri )
  data = JSON.parse( response.body )
  starting = data[0][0]
  outer =[]
  data.each do |item|
     inner = []
     inner.push( Time.at(item[0]*0.001) )#time
     inner.push( item[3] )#lo
     inner.push( item[1] )#open
     inner.push( item[4] )#close
     inner.push( item[2] )#hi
     outer.push( inner )
  end
  self.worker( outer, contract, starting )
 end#response
 def worker( outer, contract, starting )

     if contract.to_s.include? "this"
       jsonfile = "this_week-" + starting.to_s + ".json"
       jsonpublic = "public/this.json"
     elsif contract.to_s.include? "next"
       jsonfile = "next_week-" + starting.to_s + ".json"
       jsonpublic = "public/next.json"
     elsif contract.to_s.include? "quarter"
       jsonfile = "quarter-" + starting.to_s + ".json"
       jsonpublic = "public/quarter.json"
     elsif contract.to_s.include? "spot"
       jsonfile = "spot-" + starting.to_s + ".json"
       jsonpublic = "public/spot.json"
     end#inner if

     jf = open( jsonfile, 'w+')
     jf.write( outer.to_json )
     jf.close
     FileUtils.cp(jsonfile, jsonpublic)

     ## backup to mongo db ??

     ## remove file
     Fileutils.rm(jsonfile)

 end#worker


 end#class OkCoin

 # =============================
 # Cryptofa
 #

 class Cryptofa

   def parse
     tocall = "https://www.cryptofacilities.com/derivatives/api/v2/tickers"
     self.response( tocall )
   end#parse
   def response( tocall )
     puts "Calling #{ tocall }"
     uri = URI.parse( tocall )
     response = Net::HTTP.get_response( uri )
     data = JSON.parse( response.body )
     outer = []
     ticks = data['tickers']

     ticks.each do |item|
       inner = []
        if item['bid']
          inner.push( item['lastTime'] )
          inner.push( item['symbol'] )
          inner.push( item['ask'] )#buy
          inner.push( item['bid'] )#sell
        end
      outer.push( inner )
     end
     #p outer
     self.worker( outer )
   end#response
   def worker( outer )
    outer.each do |item|

       client = db_con
       col = client[:cf]
       
       if item[0]
         p item
         jsonpublic = "public/" + item[1].to_s + ".json"

         jf = open( jsonpublic, 'w+')
         jf.write( item.to_json )
         jf.close

         data_hash = {
          'date' => item[0],
          'instrument' => item[1],
          'buy' => item[2],
          'sell' => item[3]
          }

         db_put( data_hash, col )
       end
    end
   end

 end


end#module
